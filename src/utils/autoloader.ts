import { isClass } from "./util";
import { basename, extname } from 'path';
const fs = require('fs').promises;

export async function loadDir<T>(dir: string, exclude: string[] = []): Promise<T[]> {
    let modules: T[] = [];
    const supportedExtensions = ['.js', '.cjs', '.mjs'];
    if (Reflect.has(process, Symbol.for('ts-node.register.instance'))) {
        supportedExtensions.push('.ts');
    }
    let files: string[] = await fs.readdir(dir);
    for (const file of files) {
        if (exclude.some(x => basename(file) === x || file === x) ||
            !supportedExtensions.includes(extname(file)) ||
            file.endsWith(".d.ts")) continue;
        let x = await require(`${dir}/${file}`);
        if (isClass(x)) {
            modules.push(new x);
            return modules;
        }
        for (const key in x) {
            if (Object.prototype.hasOwnProperty.call(x, key)) {
                modules.push(new x[key]);
            }
        }
    }
    return modules;
}