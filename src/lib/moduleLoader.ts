import { IModulesConfig } from "../models/module";
import { loadDir } from "../utils/autoloader";
import { getRootPath } from "../utils/util";
import { Module } from "./module";

export class ModuleLoader {

    static modules: Module[] = [];

    static async init(config: IModulesConfig, path?: string, logging: boolean = false) {
        if (!path) path = getRootPath() + "/modules";
        if (logging) console.log(`[INFO | SapphireModule] Loading modules from ${path}`);
        let loadedModules = await loadDir<Module>(path);
        for (const x of loadedModules) {
            x.setConfig(config[x.name])
        }
        this.modules = loadedModules.filter(x => x instanceof Module && config[x.name] && x.validConfig()) as Module[];
        if (logging) {
            console.log(`[INFO | SapphireModule] Loaded ${this.modules.length} module(s)`);
            if (this.modules.length > 0) console.log(`[INFO | SapphireModule] Loaded module(s): ${this.modules.map(x => x.name).join(", ")}`);
        }
    }

}