import { Command } from "@sapphire/framework";
import { IModule, IModuleConfig } from "../models/module";

export abstract class Module<T extends IModuleConfig = IModuleConfig> implements IModule<T> {
    abstract name: string;
    config: T | undefined;

    commands: Command[] = [];

    setConfig(config: T) {
        this.config = config;
    }

    addCommand(command: Command) {
        this.commands.push(command);
    }

    abstract validConfig(): boolean;
}