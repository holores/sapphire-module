import { BucketScope, Command, CommandChatInputRegisterShortcut, CommandOptionsRunTypeEnum, DetailedDescriptionCommand, RegisterBehavior } from "@sapphire/framework";
import { Snowflake } from "discord.js";

export interface IModule<T extends IModuleConfig = IModuleConfig> {
    name: string;
    config: T | undefined;
    validConfig: (config: T) => boolean;
}

interface SapphireBaseOptions {
    generateDashLessAliases?: boolean;
    description?: string;
    detailedDescription?: DetailedDescriptionCommand;
    fullCategory?: string[];
    preconditions?: any[];
    quotes?: [string, string][];
    nsfw?: boolean;
    cooldownLimit?: number;
    cooldownDelay?: number;
    cooldownScope?: BucketScope;
    cooldownFilteredUsers?: Snowflake[];
    // requiredClientPermissions?: PermissionResolvable;
    // requiredUserPermissions?: PermissionResolvable;
    runIn?: Command.RunInTypes | CommandOptionsRunTypeEnum | readonly (Command.RunInTypes | CommandOptionsRunTypeEnum)[] | null;
    typing?: boolean;
    chatInputCommand?: CommandChatInputRegisterShortcut;
}

export interface IModuleConfig {
    enabled: boolean;
    commandOptions: Partial<SapphireBaseOptions>;
    slashOptions: {
        guildIds: string[] | undefined;
        behaviorWhenNotIdentical: RegisterBehavior | undefined;
        allowSlashCommands: boolean;
    }
}

export interface IModulesConfig {
    [key: string]: { [key: string]: any } & Required<IModuleConfig>;
}
