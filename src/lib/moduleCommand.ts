import { ApplicationCommandRegistry, Args, Command, RegisterBehavior } from "@sapphire/framework";
import { IModuleConfig } from "../models/module";
import { ModuleLoader } from "./moduleLoader";

export abstract class ModuleCommand<ArgType extends Args = Args> extends Command<ArgType> {
    moduleConfig: { [key: string]: any } & Required<IModuleConfig> | undefined;
    moduleName: string | undefined;

    public constructor(context: Command.Context, options: ModuleCommandOptions) {
        let assignedModule = ModuleLoader.modules.find(x => x.name === options.moduleName);
        if (assignedModule) {
            if (assignedModule.config) {
                options = {
                    ...assignedModule.config.commandOptions,
                    ...options,
                    enabled: assignedModule.config.enabled
                };
            }
            super(context, options);
            this.moduleName = options.moduleName;
            this.moduleConfig = assignedModule.config;
            assignedModule.addCommand(this);
        } else {
            super(context, { ...options, enabled: false });
            console.warn(`Invalid module name provided for ${options.name} command!`);
        }
    }

    public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
        if (!this.moduleConfig?.slashOptions.allowSlashCommands || !this.enabled) return;
        registry.registerChatInputCommand({
            name: this.name,
            description: this.description
        }, {
            guildIds: this.moduleConfig?.slashOptions.guildIds,
            behaviorWhenNotIdentical: this.moduleConfig?.slashOptions.behaviorWhenNotIdentical ?? RegisterBehavior.LogToConsole
        });
    }

}

export interface ModuleCommandOptions extends Command.Options {
    moduleName: string;
}