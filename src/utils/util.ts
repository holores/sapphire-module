import { readFileSync } from 'fs';
import { dirname, join } from 'path';

export function isClass(value: unknown) {
    return typeof value === 'function' && typeof value.prototype === 'object';
}

export function getRootPath(): String {
    const cwd = process.cwd();

    try {
        const file = JSON.parse(readFileSync(join(cwd, 'package.json'), 'utf8'));
        return dirname(join(cwd, file.main));
    } catch {
        return cwd;
    }
}